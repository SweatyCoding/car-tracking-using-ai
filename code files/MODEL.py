# inputs for CNN layer - [input shape, number of filters, kernal size, stride, padding=1]

from scipy import signal
#from layer import Layer
import torch.nn as nn
import numpy as np
import torch
from numpy import array

num_classes = 1

config = [
    (32, 3, 1),
    (64, 3, 2),
    ["B", 1],
    (128, 3, 2),
    ["B", 2],
    (256, 3, 2),
    ["B", 8],
    (512, 3, 2),
    ["B", 8],
    (1024, 3, 2),
    ["B", 4],  # To this point is Darknet-53
    (512, 1, 1),
    (1024, 3, 1),
    "S",
    (256, 1, 1),
    "U",
    (256, 1, 1),
    (512, 3, 1),
    "S",
    (128, 1, 1),
    "U",
    (128, 1, 1),
    (256, 3, 1),
    "S",
]

def convolute(firstArray, filterArray, padding, stride):
    """
    :param firstArray: Array to be convolved
    :param filterArray: Kernel for convolution
    :param padding: Padding, valid : no padding, output will shrink by kernel size - 1; same : padding with zeroes will occur, size will grow by 3 - kernel size
    :param stride: Stride, how many rows and columns traversed per slide.
    :return:
    """
    if padding == "same":
        padding = 1
    else:
        padding = 0
    outArray = np.zeros((int((firstArray.shape[0] - filterArray.shape[0] + padding*2)/stride) + 1, int((firstArray.shape[1] - filterArray.shape[1] + padding * 2)/stride) + 1)) # https://d2l.ai/chapter_convolutional-neural-networks/padding-and-strides.html
    firstArray = np.pad(firstArray, (padding, padding), 'constant', constant_values=(0, 0))
    array_h = firstArray.shape[0] - 1
    array_w = firstArray.shape[1] - 1
    kernel_h = filterArray.shape[0] - 1
    kernel_w = filterArray.shape[1] - 1
    print(firstArray, "\n")
    i = 0
    j = 0
    while i <= array_h - kernel_h:
        while j <= array_w - kernel_h:
            outArray[int(i / stride)][int(j / stride)] = conv_single_step(firstArray[i:i+kernel_h + 1, j:j+kernel_w + 1], filterArray)
            j += stride
        i += stride
        j = 0
    return outArray


def conv_single_step(input_mat, kernel):
    """
    input_mat - part of array that we want to convolve
    kernel - kernel
    """
    s = np.multiply(input_mat, kernel)
    Z = np.sum(s)
    return Z

class Convolutional():
    def __init__(self, input_shape, kernel_size, num_of_filters, stride, padding="same"): # "same" == padding=1, "valid" == padding=0
        input_depth, input_height, input_width = input_shape  # (3, 416, 416)
        self.depth = num_of_filters
        self.input_shape = input_shape
        self.input_depth = input_depth

        #add the change in output if padding is "valid"
        if padding == "valid":
            output_height = (input_height - kernel_size + 1) // stride
            output_width = (input_width - kernel_size + 1) // stride
        else:  #if padding == "same":
            output_height = input_height // stride  # rounded up
            output_width = input_width // stride  # rounded up

        self.output_shape = (num_of_filters, output_height, output_width)  # (num_of_filters,
        self.kernels_shape = (num_of_filters, input_depth, kernel_size, kernel_size)
        self.kernels = np.random.randn(*self.kernels_shape)
        self.biases = np.random.randn(*self.output_shape)
        self.padding = padding
        self.stride = stride

    def forward1(self, input, padding="same", stride=1):
        self.input = input
        self.output = np.copy(self.biases)
        for i in range(self.depth):
            for j in range(self.input_depth):
                self.output[i] += convolute(self.input[j], self.kernels[i, j], padding, stride)  #self.output[i] += signal.correlate2d(self.input[j], self.kernels[i, j], padding)  # convolute(firstArray, filterArray, padding, stride)
                #replace correlate2d with same
                # function that supports different strides since right now the stride must be 1 and the
                # padding is same so the output size is in the same size as the input. in stride=2 the
                # output is square root of the input (example: from 16X16 it becomes 4X4).
        return self.output

    def backward1(self, output_gradient, learning_rate, padding="same", stride=1):
        kernels_gradient = np.zeros(self.kernels_shape)
        input_gradient = np.zeros(self.input_shape)

        for i in range(self.depth):
            for j in range(self.input_depth):
                kernels_gradient[i, j] = signal.correlate2d(self.input[j], output_gradient[i], padding)
                input_gradient[j] += signal.convolve2d(output_gradient[i], self.kernels[i, j], padding)  # should be "full"

        self.kernels -= learning_rate * kernels_gradient
        self.biases -= learning_rate * output_gradient
        return input_gradient

class CNNBlock():
    def __init__(self, kernel_size, num_of_filters, stride, batch, padding="same", bn_act=True):
        self.conv = Convolutional(batch[0].shape, kernel_size, num_of_filters, stride, padding)
        self.bn = nn.BatchNorm2d(num_of_filters)  # should be TensorFlow function

        def ReLU(Z, negetive_slope=0.1):
            return np.maximum(Z, 0) + negetive_slope * np.minimum(Z, 0)
        self.leaky = nn.ReLU(0.1)
        self.use_bn_act = bn_act
        self.padding = padding
        self.stride = stride
        self.num_of_filters = num_of_filters
        self.kernel_size = kernel_size

    def forward1(self, info):  # x is the input array or image
        if self.use_bn_act:
            return self.leaky(self.bn(self.conv.forward1(info, self.padding, self.stride)))
        else:
            return self.conv.forward1(info, self.padding, self.stride)

    def backward1(self, output_gradient, learning_rate):
        return self.conv.backward1(output_gradient, learning_rate, self.padding, self.stride)

    def insert_batch_to_convolve(self, batch):
        self.conv = Convolutional(batch[0].shape, self.kernel_size, self.num_of_filters, self.stride, self.padding)

class ResidualBlock():
    # (self, kernel_size, num_of_filters, stride, batch, padding="same", bn_act=True)
    #in_channels here are the amount of filters that was applied in the previous layer. therefore batch[0].shape[0] will be in_channels which is the depth of the image.
    # The out_channels will be in_channels//2 and then in_channels back again
    def __init__(self, stride, batch, use_residual=True, num_repeats=1):
        super().__init__()
        self.layers = []
        for repeat in range(num_repeats):
            self.layers.append([
                            CNNBlock(1, batch[0].shape[0]//2, stride, batch),  # kernel size of 1
                            CNNBlock(3, batch[0].shape[0]*2, stride, batch)  # kernel size of 3
                           ])


        self.use_residual = use_residual
        self.num_repeats = num_repeats

    def forward1(self, x):
        for residualBlock in self.layers:  #for every residual block

            if self.use_residual:  # implement skip connection
                x = x + residualBlock[1].forward1(residualBlock[0].forward1(x))  # for every conv layer in residual block (there are 2..)
            else:  # leave it as is
                x = residualBlock[1].forward1(residualBlock[0].forward1(x))  # for every conv layer in residual block (there are 2..)

        return x

    def backward1(self, output_gradient, learning_rate):
        for residualBlock in reversed(self.layers):  #for every residual block in reverse [r1, r2] -> [r2, r1] == [[c1, c2], [c3, c4]] -> [[c3, c4], [c1, c2]]
            for layer_conv in reversed(residualBlock):  # will be -> [c4.backward1(), c3.backward1(), c2.backward1(), c1.backward1()]
                output_gradient = layer_conv.backword1(output_gradient, learning_rate)  # update the gradient from each conv block

        return output_gradient  # return the last gradient (in the example it will be return c1.backward1())

    def insert_batch_to_convolve(self, batch):
        for residualBlock in self.layers:  # for every residual block in layers
            for layer_conv in residualBlock:
                layer_conv.insert_batch_to_convolve(batch)

class ScalePrediction():
    def __init__(self, stride, batch):
        self.num_classes_self = num_classes
        self.layers = [
            CNNBlock(3, batch[0].shape[0] * 2, stride, batch, padding="same"),
            CNNBlock(1, 3 * (self.num_classes_self + 5), stride, batch, bn_act=False)  # out channels will be 3 anchors * [car, probability that there is is any object in the image, x0, y0, x1, y1] (len 5)
        ]
        #(self, kernel_size, num_of_filters, stride, batch, padding="same", bn_act=True)

    def forward1(self, x):
        x = self.layers[1].forward1(self.layers[0].forward1(x)).reshape(x.shape[0], 3, self.num_classes_self + 5, x.shape[2], x.shape[3]).permute(0, 1, 3, 4, 2)  # permute change the order of parameters from reshape
        # for every conv layer in scale prediction (there are 2..)

        return x

    def backward1(self, output_gradient, learning_rate):
        output_gradient = self.layers[1].backward1(output_gradient, learning_rate) # update the gradient from each conv layer
        output_gradient = self.layers[0].backward1(output_gradient, learning_rate) # update the gradient from each conv layer

        return output_gradient  # return the last gradient (in the example it will be return self.layers[0].backword1()

    def insert_batch_to_convolve(self, batch):
        for layer in self.layers:
            layer.insert_batch_to_convolve(batch)

class YOLOv3(nn.Module):
    def __init__(self, in_channels=3, num_classes_self=1):
        self.num_classes_self = num_classes_self
        self.in_channels = in_channels
        self.layers = self.create_conv_layers()

    def forward1(self, batch):
        outputs = []  # for each scale
        route_connections = []
        for layer in self.layers:
            if isinstance(layer, ScalePrediction):
                layer.insert_batch_to_convolve(batch)
                outputs.append(layer.forward1(batch))
                continue

            layer.insert_batch_to_convolve(batch)
            batch = layer.forward1(batch)

            if isinstance(layer, ResidualBlock) and layer.num_repeats == 8:
                route_connections.append(batch)

            elif isinstance(layer, nn.Upsample):
                batch = torch.cat([batch, route_connections[-1]], dim=1)
                route_connections.pop()

        return outputs

    def create_conv_layers(self):
        layers = []
        in_channels = self.in_channels

        for layer in config:
            batch = [np.zeros((in_channels, 1, 1))]  # just for initialization

            if isinstance(layer, tuple):
                out_channels, kernel_size, stride = layer
                layers.append(CNNBlock(kernel_size, out_channels, stride, batch, padding="same" if kernel_size == 3 else 0))

                in_channels = out_channels

            elif isinstance(layer, list):
                num_repeats = layer[1]
                stride = 2
                layers.append(ResidualBlock(stride, batch, num_repeats=num_repeats))
                # (stride, batch, use_residual=True, num_repeats=1)

            elif isinstance(layer, str):
                if layer == "S":
                    stride = 2
                    batch = [np.zeros((in_channels // 2, 1, 1))]  # just for initialization
                    layers += [
                        ResidualBlock(stride, batch, num_repeats=1, use_residual= False),
                        CNNBlock(1, in_channels // 2, stride, batch),  # CNNBlock(kernel_size, out_channels, stride, batch)
                        ScalePrediction(stride, batch),  #(stride, batch)
                    ]
                    in_channels = in_channels // 2

                elif layer == "U":
                    layers.append(nn.Upsample(scale_factor=2), )
                    in_channels = in_channels * 3

        return layers

    def backward1(self, output_gradient, learning_rate):
        pass


if __name__ == "__main__":

    IMAGE_SIZE = 416
    model = YOLOv3()
    x = torch.randn((3, IMAGE_SIZE, IMAGE_SIZE))
    out = model.forward1([x])
    #assert model(x)[0].shape == (2, 3, IMAGE_SIZE//32, IMAGE_SIZE//32, num_classes + 5)
    #assert model(x)[1].shape == (2, 3, IMAGE_SIZE//16, IMAGE_SIZE//16, num_classes + 5)
    #assert model(x)[2].shape == (2, 3, IMAGE_SIZE//8, IMAGE_SIZE//8, num_classes + 5)
    print("Success!")