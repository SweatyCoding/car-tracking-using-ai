from bs4 import BeautifulSoup
import cv2
import np
import scipy.io
import unidecode
# import required module
from pathlib import Path
# Importing Image class from PIL module
from PIL import Image, ImageOps
# get the path/directory
folder_dir = 'imgs'

#new_locations = [[{"viechle": [1, 2, 3, 4]}], [], [], [], [], []]
#for the first - no change | second: x -= (x_max / 3) | third: x -= (x_max / 3 *2) | fourth: y -= (y_max / 2) |
# | fifth: y -= (y_max / 2) and x -= (x_max / 3) | sixth: y -= (y_max / 2) and x -= (x_max / 3 *2)

#specs of cropped image
x_length = 640
y_length = 540

#################################################################################
#############################  FUNCTIONS  #######################################
#################################################################################

# fun returns all the objects in the xml file
def read_xml(file_name):
    #'dict.xml'
    # Reading the data inside the xml
    # file to a variable under the name
    # data
    with open(file_name, 'r') as f:
        data = f.read()

    # Passing the stored data inside
    # the beautifulsoup parser, storing
    # the returned object
    Bs_data = BeautifulSoup(data, "xml")

    # Finding all instances of tag `object`
    b_objects = Bs_data.find_all('object')

    return b_objects

#function return the name of the object
def extract_name_from_object(obj):
    name_1 = obj.find('name')
    for a in name_1:
        return str(a.encode('utf-8'))[2:-1] # convert to string and slice

# fun return the bounding box ([lest, right, top, bottom]) from an object
def extract_boundingBox_from_object(obj):
    bnd_box = obj.find('bndbox')
    sec = []
    coordinates = [bnd_box.find('xmin'), bnd_box.find('xmax'), bnd_box.find('ymin'), bnd_box.find('ymax')]
    for cor in coordinates:
        for a in cor:
            sec.append(a)
            break

    return [extract_number_from_str(sec[0]), extract_number_from_str(sec[1]), extract_number_from_str(sec[2]), extract_number_from_str(sec[3])]

# return int number from the string
def extract_number_from_str(str1):
    num = 0
    for let in str1:

        if let.isdigit():
            num = num*10 + (ord(let) - ord('0'))
    return num

#function change the coordinates of the bounding box so it will fit from the original image to the new one (smaller in 1/6 from original picture)
def change_bndBox(num_of_bndBox, values1):
    values = values1.copy()

    if num_of_bndBox == 1:
        return values
    if num_of_bndBox == 2:
        values[0] = values[0] - x_length
        values[1] = values[1] - x_length
        return values

    if num_of_bndBox == 3:
        values[0] = values[0] - (x_length*2)
        values[1] = values[1] - (x_length*2)
        return values

    if num_of_bndBox == 4:
        values[2] = values[2] - y_length
        values[3] = values[3] - y_length
        return values

    if num_of_bndBox == 5:
        values[2] = values[2] - y_length
        values[3] = values[3] - y_length
        values[0] = values[0] - x_length
        values[1] = values[1] - x_length
        return values

    if num_of_bndBox == 6:
        values[2] = values[2] - y_length
        values[3] = values[3] - y_length
        values[0] = values[0] - (x_length * 2)
        values[1] = values[1] - (x_length * 2)
        return values

    return 0

#function see if after redoing the coordinates for section_num section the bndBox is still partlly inside of the section. if yes - it recreate all the values that are not inside the section to the borders of the section
def create_ignore_box(bndBox, section_num):

    new_bndBox = change_bndBox(section_num, bndBox)

    if new_bndBox[0] >= x_length or new_bndBox[1] <= 0 or new_bndBox[2] >= y_length or new_bndBox[3] <= 0:
        return [-1, -1, -1, -1]

    if not(new_bndBox[0] >= 0 and new_bndBox[0] <= x_length):
        new_bndBox[0] = 0

    if not(new_bndBox[1] >= 0 and new_bndBox[1] <= x_length):
        new_bndBox[1] = x_length

    if not (new_bndBox[2] >= 0 and new_bndBox[2] <= y_length):
        new_bndBox[2] = 0

    if not(new_bndBox[3] >= 0 and new_bndBox[3] <= y_length):
        new_bndBox[3] = y_length

    return new_bndBox


#function calculate the section that the partly square is belong to
def check_what_ignore_belong_to(prefer, section):
    # section == [left, right, top, bottom]
    sec_1 = [0, 640, 0, 540]
    sec_2 = [640, 1280, 0, 540]
    sec_3 = [1280, 1920, 0, 540]
    sec_4 = [0, 640, 540, 1080]
    sec_5 = [640, 1280, 540, 1080]
    sec_6 = [1280, 1920, 540, 1080]

    if section[3] <= sec_1[3]:  # first floor

        if prefer == 1:
            if section[0] <= sec_1[1]:  # first section
                return 1
        if prefer == 2:
            if section[1] >= sec_2[0] and section[1] < sec_2[1]:
                return 2
            if section[0] < sec_2[1] and section[0] > sec_2[0]:
                return 2
        if prefer == 3:
            if section[1] > sec_3[0]:
                return 3

    elif section[2] >= sec_4[2]:  # second floor
        if prefer == 4:
            if section[0] <= sec_1[1]:  # first section
                return 4
        if prefer == 5:
            if section[1] >= sec_2[0] and section[1] < sec_2[1]:
                return 5
            if section[0] < sec_2[1] and section[0] > sec_2[0]:
                return 5
        if prefer == 6:
            if section[1] > sec_3[0]:
                return 6

    return 7  # ignore - not belong to any section or not relevant to my current section

# function check if the ignored bndBox is more than 70% of the original bndBox. if yes - it return True meaning we wont ignore the box. otherwise (less than 70%) we will ignore it by returning False
def if_small_enough_to_ignore(original_bndBox, new_ignore_bndBox):
    percentage = 0.8

    x_original = original_bndBox[1] - original_bndBox[0] # x_max - x_min
    y_original = original_bndBox[3] - original_bndBox[2] #y_max - y_min

    x_new = new_ignore_bndBox[1] - new_ignore_bndBox[0] # x_max - x_min
    y_new = new_ignore_bndBox[3] - new_ignore_bndBox[2] #y_max - y_min

    if x_new * y_new >= x_original * y_original * percentage: # if greater than 70% from original
        return True
    return False

# function get 4 coordinates and returns the specific section that the it is in
def what_section(section):
    #section == [left, right, top, bottom]
    sec_1 =  [0, 640, 0, 540]
    sec_2 = [640, 1280, 0, 540]
    sec_3 = [1280, 1920, 0, 540]
    sec_4 = [0, 640, 540, 1080]
    sec_5 = [640, 1280, 540, 1080]
    sec_6 = [1280, 1920, 540, 1080]

    if section[2] >= sec_1[2] and section[3] <= sec_1[3]: #first floor

        if section[0] >= sec_1[0] and section[1] <= sec_1[1]: # first section
            return 1

        if section[0] >= sec_2[0] and section[1] <= sec_2[1]: # second section
            return 2

        if section[0] >= sec_3[0] and section[1] <= sec_3[1]: # third section
            return 3

    elif section[2] >= sec_4[2] and section[3] <= sec_4[3]: # second floor

        if section[0] >= sec_4[0] and section[1] <= sec_4[1]:  # first section
            return 4

        if section[0] >= sec_5[0] and section[1] <= sec_5[1]:  # second section
            return 5

        if section[0] >= sec_6[0] and section[1] <= sec_6[1]:  # third section
            return 6

    return 7 # ignore - not belong to any section

########################################################################################
#################################   MAIN    ############################################
########################################################################################

# iterate over files in that directory
images = Path(folder_dir).glob('*.jpg')
i=0
counter = 0
for image in images:

    all_bndBoxes = []
    # create the eqivalent annotation file name
    img_name_xml_file = 'annotations/' + image.name[0:-3] + 'xml'
    objects = read_xml(img_name_xml_file) # get all objects in that file


    for obj in objects:
        all_bndBoxes.append(extract_boundingBox_from_object(obj)) # get all bounding boxes in that file (of vehicles...)


    # Opens a image in RGB mode
    im = Image.open(image)
    # Size of the image in pixels (size of original image)
    # (This is not mandatory)
    width, height = im.size
    i += 1 # to give names of frames
    counter = 0

    for k in range(0, 2):
        for j in range(0, 3):

            new_bndBox_arr = [] # collect all bndBox that in that section
            dict_obj = {} # represent single vehicle object - {"name" : "Veichle" , "ignore" : 0 , "bndBox" : [1, 2, 3, 4]}

            counter += 1 # 1-6 images
            # Setting the points for cropped image
            left = j * x_length
            top = k * y_length
            right = left + x_length
            bottom = top + y_length

            # Cropped image of above dimension
            # (It will not change original image)
            im1 = im.crop((left, top, right, bottom))

            serial_number_img = image.name[3:5] + '_frame' + str(i) + "_" + str(counter) + ".jpg" #new name for image file
            serial_number_xml = image.name[3:5] + '_frame' + str(i) + "_" + str(counter) + ".txt" #new name for annotation file

            path1 = r"cropped\\" + serial_number_img # directory path
            im1.save(path1) #save new picture

            #convert PIL Image object to a Mat openCV object
            image_np = np.array(im1)
            image_np = cv2.cvtColor(image_np, cv2.COLOR_RGBA2RGB)

            # iterate all bounding boxes in original image to see if they are belong to the current section or not. if yes - add them to array
            for tp in range(0, len(all_bndBoxes)):
                dict_obj = {}
                bndBox = all_bndBoxes[tp]
                curr_section = what_section(bndBox)
                if curr_section == counter: # if same section
                    new_bndBox = change_bndBox(counter, bndBox) # calculate new bounding_box

                    #draw new box
                    start_point = (new_bndBox[0], new_bndBox[2])
                    end_point = (new_bndBox[1], new_bndBox[3])
                    image_np = cv2.rectangle(image_np, start_point, end_point, (255, 0, 0), 2) #draw on Mat

                    #create the dict representing the object in picture
                    dict_obj["bndBox"] = new_bndBox
                    dict_obj["name"] = extract_name_from_object(objects[tp])
                    dict_obj["ignore"] = 0

                    new_bndBox_arr.append(dict_obj) # add it to array


                elif curr_section == 7: #if ignore

                    partlly_belong_to = check_what_ignore_belong_to(counter, bndBox)
                    if partlly_belong_to != 7:
                        ignore_bndBox = create_ignore_box(bndBox, partlly_belong_to) # create new ignore box that fit to this section

                        big_enough_to_not_ignore = if_small_enough_to_ignore(bndBox, ignore_bndBox) # return True or False
                        if -1 not in ignore_bndBox: # if relevant
                            if big_enough_to_not_ignore:
                                #draw new box
                                start_point = (ignore_bndBox[0], ignore_bndBox[2])
                                end_point = (ignore_bndBox[1], ignore_bndBox[3])
                                image_np = cv2.rectangle(image_np, start_point, end_point, (0, 0, 255), 2) #draw on Mat

                                # create the dict representing the object in picture
                                dict_obj["bndBox"] = ignore_bndBox
                                dict_obj["name"] = extract_name_from_object(objects[tp])
                                dict_obj["ignore"] = 0 # not ignore

                            else:
                                # draw new box
                                start_point = (ignore_bndBox[0], ignore_bndBox[2])
                                end_point = (ignore_bndBox[1], ignore_bndBox[3])
                                image_np = cv2.rectangle(image_np, start_point, end_point, (0, 255, 0), 2)  # draw on Mat

                                # create the dict representing the object in picture
                                dict_obj["bndBox"] = ignore_bndBox
                                dict_obj["name"] = extract_name_from_object(objects[tp])
                                dict_obj["ignore"] = 1  # ignore

                            new_bndBox_arr.append(dict_obj)  # add it to array


            #create path for new imgs with the sqaures on them
            path2 = r"crroped_imgs_with_squres\\" + serial_number_img # file including directory
            # convert openCV Mat to PIL Image
            im_pil = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
            im_pil = Image.fromarray(im_pil)
            im_pil.save(path2) #save new image

            #create new xml file and write his data!! - txt file in json format - { "list" : [{"name" : "Vehicle" , "ignore" : 0 , "bndBox" : [1, 2, 3, 4]}, ... ] }
            labeling = {}
            labeling["list"] = new_bndBox_arr # create final json format with all the data

            path3 = r"cropped_annotations\\" + serial_number_xml # file including directory
            with open(path3, 'w') as f: # open new file
                f.write(str(labeling)) # write new data
