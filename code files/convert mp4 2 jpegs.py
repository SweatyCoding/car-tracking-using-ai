# import required module
import os
import cv2
import time
import numpy as np

IMG_SIZE = 416
def crop_image(image):
    image = np.array(image)

    im1 = image[50:450, 300:860]
    im1 = cv2.resize(im1, (IMG_SIZE, IMG_SIZE), cv2.INTER_AREA)
    return im1

def mp4_2_jpgs(video_path, counter, count4):
    p = True
    while p:
        try:
            vidcap = cv2.VideoCapture(video_path)
            success, image = vidcap.read()
            p = False
        except OSError as e:
            time.sleep(2)

    count = counter
    count2 = 0
    count3 = 1
    arr = [3, 6, 9] # array of numbers to be saved (the len of array will be the number of saved photos from 10 photos).
                    # example: [3, 6, 9] will save 3/10 photos per second. [2, 4, 6, 8] will save 4/10.
    while success:
        if count != 1 and count % 200 == 1 and count2 != 0:
            break
        if count3 in arr:
            path = dir_path2 + "\\" + str(count4) + ".jpg"
            #print(image.shape)
            im1 = crop_image(image)
            cv2.imwrite(path, im1)  # save frame as JPEG file
            count4 += 1
        success, image = vidcap.read()
        count += 1
        count2 += 1
        count3 += 1
        if count3 == 11:
            count3 = 1
    return count, count4

# assign directory
dir_path = r"C:\Users\Owner\Videos\AI_Parking"
dir_path2 = r"C:\Users\Owner\Videos\AI_Parking_photos"
textfilepath = r"C:\Users\Owner\Documents\magshimim\final_year_project\OBS SCRIPTS\count+count4.txt"
# iterate over files in that directory

#read the values of last counter and count4
f = open(textfilepath, "r")
k = f.readlines()

counter = int(k[0].split(":")[1])
count4 = int(k[1].split(":")[1])
f.close()

while True:
    if not os.listdir(dir_path):
        time.sleep(2) # Sleep for 2 seconds
    else:
        for filename in os.listdir(dir_path):
            #if os.path.exists(filename):
                try:
                    full_path = os.path.join(dir_path, filename)
                    counter, count4 = mp4_2_jpgs(full_path, counter, count4)

                    #write new values of counter and count4
                    f = open(textfilepath, "w")
                    content = "counter:" + str(counter) +"\ncount4:" + str(count4)
                    f.write(content)

                    os.remove(full_path)
                except OSError as e:
                    time.sleep(21) # Sleep for 21 seconds









