from roboflow import Roboflow

# fixes in labeling (where the model wasnt good) - (11161-11188), (10850-10852), (10387-10484), (8919-8984), (7901-8773), (13057-13063), (13290-13293), (14278-14279), (15421-15451), (18287-18294), (18379-18416)

annot_dir = "C:\\Users\\Owner\\Documents\\magshimim\\final_year_project\\ai-parking-dataset\\AI_Parking_photos_labels\\"
labeled_imgs_dir = "C:\\Users\\Owner\\Documents\\magshimim\\final_year_project\\ai-parking-dataset\\labeled_imgs\\"
imgs_dir = 'C:\\Users\\Owner\\Videos\\AI_Parking_photos\\'

#Load model
rf = Roboflow(api_key="ErHzp6fL3mmaSliWgJto")  #ErHzp6fL3mmaSliWgJto
project = rf.workspace().project("ai-parking_4")  #ai-parking
model = project.version(1).model

#(x1, y1) and width (w) and height (h)
for i in range(102334, 113761): #last to check - 21001h
    data = ""

    # run predictions on local images
    img_name = str(i) + ".jpg"
    prediction = model.predict(imgs_dir + img_name, confidence=40, overlap=30)  #/content/sample_data/234.jpg #/content/sample_data/32558.jpg
    print(i)

    for bnd_box in prediction.json()['predictions']:
        line = "1 " + str(bnd_box['x'] / 416) + " " + str(bnd_box['y'] / 416) + " " + str(bnd_box['width'] / 416) + " " + str(bnd_box['height'] / 416) + "\n"
        data += line
    with open(annot_dir + str(i) + ".txt", "w") as f:
        f.write(data)

    # visualize your prediction
    prediction.save(labeled_imgs_dir + img_name)


# #plot
# prediction.plot()

# infer on an image hosted elsewhere
# print(model.predict("URL_OF_YOUR_IMAGE", hosted=True, confidence=40, overlap=30).json())