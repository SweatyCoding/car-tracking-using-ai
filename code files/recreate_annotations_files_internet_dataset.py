from pathlib import Path
import os
import json

folder_dir = "for_split/"
imgs_dir = "cropped"
annot_dir = "cropped_annotations"

def reannotate():
    # convert cropped_annotations from json to normal annotation:
    # txt file - each row - bounding box
    #obj 1: 'class x y w h ignore'
    #obj 2: 'class x y w h ignore'
    for dir, subdirs, files in os.walk(annot_dir):
        for f in files:
            print(f)
            all_lines = ""
            with open(os.path.join(annot_dir, f)) as txt_file:
                jsonString = txt_file.readline()
                jsonString = jsonString.replace("'", '"', jsonString.count("'"))
                aDict = json.loads(jsonString)
            for bnd_box in aDict['list']:
                new_bndBox = [bnd_box['bndBox'][0] / 640, bnd_box['bndBox'][2] / 540, (bnd_box['bndBox'][1] - bnd_box['bndBox'][0]) / 640, (bnd_box['bndBox'][3] - bnd_box['bndBox'][2]) / 540]
                new_line = "1 " + str(bnd_box['bndBox'][0] / 640) + " " + str(bnd_box['bndBox'][2] / 540) + " " + str((bnd_box['bndBox'][1] - bnd_box['bndBox'][0]) / 640) + " " + str((bnd_box['bndBox'][3] - bnd_box['bndBox'][2]) / 540) + " " + str(bnd_box['ignore'])
                all_lines += new_line
                all_lines += "\n"
                #print(bnd_box['bndBox'])
                #print("1 " + str(new_bndBox[0] * 640) + " " + str(new_bndBox[1] * 540) + " " + str(new_bndBox[2] * 640) + " " + str(new_bndBox[3] * 540) + " " + str(bnd_box['ignore']))
            with open(os.path.join(annot_dir, f), 'w') as txt_file:
                txt_file.write(all_lines[:len(all_lines)-1])


def rename(start):
    # rename all files in cropped, cropped_annotations to 1,2,3,4........
    counter = start
    for dir, subdirs, files in os.walk(imgs_dir):
        for f in files:
            f_new = str(counter) + ".jpg"
            print(f + " -> " + f_new)
            os.rename(os.path.join(imgs_dir, f), os.path.join(imgs_dir, f_new))
            counter += 1

    counter = start
    for dir, subdirs, files in os.walk(annot_dir):
        for f in files:
            f_new = str(counter) + ".txt"
            print(f)
            os.rename(os.path.join(annot_dir, f), os.path.join(annot_dir, f_new))
            counter += 1

rename(1)
reannotate()