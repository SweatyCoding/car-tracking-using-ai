import random
import csv

a = range(1, 371377)
a = list(a)
p = True

# open the file in the write mode
f = open('C:\\Users\\Owner\\Documents\\magshimim\\final_year_project\\dataset\\train.csv', 'w', newline='')
h = open('C:\\Users\\Owner\\Documents\\magshimim\\final_year_project\\dataset\\test.csv', 'w', newline='')
# create the csv writer
writer_train = csv.writer(f)
writer_test = csv.writer(h)

for i in range(1, 371377):
    num = random.choice(a)
    a.remove(num)
    img = str(num) + ".jpg"
    lbl = str(num) + ".txt"

    data = [img, lbl]
    print(i)
    if (i==258000):  # if 0.7 from whole dataset
        p = False

    if (p): # write to train.csv
        # write a row to the csv file
        writer_train.writerow(data)

    else: #write to test.csv
        # write a row to the csv file
        writer_test.writerow(data)

# close the files
f.close()
h.close()