import re
import cv2
from PIL import Image, ImageOps
import np
from pathlib import Path
colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (204, 0, 204), (0, 255, 255), (255, 0, 255), (255, 255, 0), (0, 0, 0), (255, 255, 255), (13, 13, 13), (99, 99, 199)]


def findCarsInFrame(frameNum):
    frameCarList = []
    subStr = "bndBox"
    file_name = "C:\\Users\\Owner\\Documents\\magshimim\\final_year_project\\dataset\\original\\cropped_annotations\\01_frame" + str(frameNum) + "_3.txt"
    with open(file_name) as frame_info:
        frameText = frame_info.readline()
        result = [_.start() for _ in re.finditer(subStr, frameText)]
    for r in result:
        r += 10
        coordinates = [0, 0, 0, 0]
        for i in range(0, 3, 1):
            coordinates[i] = int(frameText[r:frameText.find(', ', r)])
            r = frameText.find(', ', r) + 2
        coordinates[3] = int(frameText[r:frameText.find(', ', r) - 1])
        if len(frameCarList) == 0:
            frameCarList.append({'carNum': 1, 'minX': coordinates[0], 'maxX': coordinates[1], 'minY': coordinates[2], 'maxY': coordinates[3]})
        else:
            frameCarList.append({'carNum': frameCarList[-1]['carNum'] + 1, 'minX': coordinates[0], 'maxX': coordinates[1], 'minY': coordinates[2], 'maxY': coordinates[3]})
    return frameCarList


def checkIfCarsStacked(car1, nowCar):
    x_left = max(nowCar['minX'], car1['minX'])
    x_right = min(nowCar['maxX'], car1['maxX'])
    y_bottom = max(nowCar['minY'], car1['minY'])
    y_top = min(nowCar['maxY'], car1['maxY'])
    intersection_area = (x_right - x_left) * (y_top - y_bottom)
    nowCarArea = (nowCar['maxX'] - nowCar['minX']) * (nowCar['maxY'] - nowCar['minY'])
    car1Area = (car1['maxX'] - car1['minX']) * (car1['maxY'] - car1['minY'])
    iou = intersection_area / float(car1Area + nowCarArea - intersection_area)  # intersection over union
    if iou > 0:
        return True
    print('\n', iou, car1, nowCar)
    return False

def calculate_IOU(car1, nowCar):
    x_left = max(nowCar['minX'], car1['minX'])
    x_right = min(nowCar['maxX'], car1['maxX'])
    y_bottom = max(nowCar['minY'], car1['minY'])
    y_top = min(nowCar['maxY'], car1['maxY'])
    intersection_area = (x_right - x_left) * (y_top - y_bottom)
    nowCarArea = (nowCar['maxX'] - nowCar['minX']) * (nowCar['maxY'] - nowCar['minY'])
    car1Area = (car1['maxX'] - car1['minX']) * (car1['maxY'] - car1['minY'])
    iou = intersection_area / float(car1Area + nowCarArea - intersection_area)  # intersection over union
    return iou

def drawSquares(carList, frameNum):
    i = 0
    image = "C:\\Users\\Owner\\Documents\\magshimim\\final_year_project\\dataset\\original\\cropped\\01_frame" + str(frameNum) + "_3.jpg"
    print(image)
    print('\n')
    im1 = Image.open(image)
    image_np = np.array(im1)
    image_np = cv2.cvtColor(image_np, cv2.COLOR_RGBA2RGB)
    for car in carList:
        start_point = (car['minX'], car['minY'])
        end_point = (car['maxX'], car['maxY'])
        image_np = cv2.rectangle(image_np, start_point, end_point, colors[i], 2)
        i += 1
    # convert openCV Mat to PIL Image
    im_pil = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(im_pil)
    im_pil.save("C:\\Users\\Owner\\Documents\\magshimim\\final_year_project\\motion detection\\new\\examples\\" + str(frameNum) + ".jpg")  # save new image

def remove_unneccesary_bndBox(carList, carsInFrame):
    new_carList = []
    for bndBox in carList:
        p = False
        for curr_bndBox in carsInFrame:
            if calculate_IOU(bndBox, curr_bndBox) > 0.8:
                p = True
                break
        if p:
            new_carList.append(bndBox)
    return new_carList
def main():
    carList = []
    carList2 = []
    for frame in range(1, 50):
        carsInFrame = findCarsInFrame(frame)
        print(carsInFrame)
        if len(carList) == 0:
            carList = carsInFrame
        else:
            i = 0
            for currCar in carsInFrame:
                j = 0
                for car in carList:
                    if (currCar['minX'] in range(car['maxX'], car['minX'], -1) or currCar['maxX'] in range(car['maxX'], car['minX'], -1) or
                       car['maxX'] in range(currCar['minX'], currCar['maxX'], 1) or car['maxX'] in range(currCar['minX'], currCar['maxX'], 1)) and \
                       (currCar['minY'] in range(car['maxY'], car['minY'], -1) or currCar['maxY'] in range(car['maxY'], car['minY'], -1)):
                        if checkIfCarsStacked(car, currCar):
                            carList[j] = carsInFrame[i]
                            #carList2.append(carsInFrame[i])
                            carsInFrame[i]['carNum'] = j + 1
                            break
                    j += 1
                i += 1
        if len(carsInFrame) > len(carList):
        #if len(carsInFrame) > len(carList2):
            for car in carsInFrame:
                #if car not in carList:
                if car not in carList:
                    #car['carNum'] = carList[-1]['carNum'] + 1
                    car['carNum'] = carList[-1]['carNum'] + 1
                    carList.append(car)

        #carList = remove_unneccesary_bndBox(carList, carsInFrame)
        print(carList)

        drawSquares(carList, frame)

        #carList2 = []


if __name__ == "__main__":
    main()
