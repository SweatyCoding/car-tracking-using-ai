from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
import cv2

counter = 260
path_folder = r"C:\\Users\\Owner\\Videos\\AI_Parking_photos\\"
folder_to_save = r"C:\\Users\\Owner\\Videos\\labels\\"
mean_img_path = "mean_img.npy"
end = ".jpg"  # .jpg
labeled_images_dir = r"C:\\Users\\Owner\\Videos\\labeled_images\\"
#labeled_images

arr = np.zeros((416, 416, 3))
background = np.load(mean_img_path)
MIN_SURROUNDING_PIXELS = 20
QUALITY_OF_SURROUNDING_PIXELS = 0.00

def draw_rectangles(labels_file_name):
    path2 = labeled_images_dir + labels_file_name  # file including directory
    name = path2.split("\\")[len(path2.split("\\")) - 1]

    new_name = labels_file_name[:-4] + ".jpg"

    image_np = np.array(Image.open(path_folder+name[:-4] + ".jpg"))
    image_np = cv2.cvtColor(image_np, cv2.COLOR_RGBA2RGB) #.astype('float32')

    with open(labels_file_name, 'r') as txt_file:
        data = txt_file.readlines()

        for line in data:
            bndbox = line.split(" ")[1:]
            # draw new box
            start_point = (int(bndbox[0]), int(bndbox[1]))
            end_point = (int(bndbox[0])+int(bndbox[2]), int(bndbox[1])+int(bndbox[3]))
            # print(start_point)
            # print(end_point)
            image_np = cv2.rectangle(image_np, start_point, end_point, (0, 255, 0), 2)  # draw on Mat


    # create path for new imgs with the sqaures on them
    path2 = labeled_images_dir + path2.split("\\")[len(path2.split("\\"))-1][:-4]+".jpg"

    # convert openCV Mat to PIL Image
    im_pil = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(im_pil)
    im_pil.save(path2)  # save new image

def classification_file_axis_checker(arrToCheck, minX, minY, maxX, maxY, value):
    jmpX = -1
    jmpY = -1
    if maxX - minX > 0:
        jmpX = 1
    if maxY - minY > 0:
        jmpY = 1
    if value == 1:
        for x in range(minX, maxX, jmpX):
            for y in range(minY, maxY, jmpY):
                for z in range(0, 3):
                    counter2 = 0
                    if arrToCheck[x, y, z] != 0:
                        for x2 in range(x - 5, x + 5):
                            for y2 in range(y - 5, y + 5):
                                for z2 in range(0, 3):
                                    if arrToCheck[x2, y2, z] > QUALITY_OF_SURROUNDING_PIXELS:
                                        counter2 += 1
                        if counter2 > MIN_SURROUNDING_PIXELS:
                            return x
    elif value == 2:
        for y in range(minY, maxY, jmpY):
            for x in range(minX, maxX, jmpX):
                for z in range(0, 3):
                    counter2 = 0
                    if arrToCheck[x, y, z] != 0:
                        for x2 in range(x - 5, x + 5):
                            for y2 in range(y - 5, y + 5):
                                for z2 in range(0, 3):
                                    if arrToCheck[x2, y2, z] > QUALITY_OF_SURROUNDING_PIXELS:
                                        counter2 += 1
                    if counter2 > MIN_SURROUNDING_PIXELS:
                        return y


def create_classification_file(arr, number_of_file):
    """
    Function takes a segmented image and create classification file (bounding boxes on cars) with the name: number_of_file.txt
    Their content for example:
    class_id x y w h
    1 12 405 28 303
    1 0.334 0.788 0.5734 0.88563
    :param arr:the numpy array that represent the segmented image
    :param number_of_file: number_of_file.txt
    :return:
    """
    name_of_file = str(number_of_file) + ".txt"

    # first find leftmost car pixel
    minX = classification_file_axis_checker(arr, 150, 150, 300, 350, 1)
    # now find top
    minY = classification_file_axis_checker(arr, 150, 150, 300, 350, 2)
    # now find rightmost
    maxX = classification_file_axis_checker(arr, 300, 150, 150, 350, 1)
    # now find bottom
    maxY = classification_file_axis_checker(arr, 150, 350, 300, 150, 2)
    width = maxX - minX
    height = maxY - minY
    f = open(folder_to_save + name_of_file, "w")
    f.write(str(number_of_file) + " " + str(minX) + " " + str(minY) + " " + str(width) + " " + str(height))
    f.close()

    draw_rectangles(folder_to_save+name_of_file)


for i in range(225, counter):  # Sum all the pictures in arr
    # 663 - example in night
    # 41951 - example in day
    # (1, 113761):
    print("1: " + str(i))
    img = Image.open(path_folder + str(i) + end)
    np_img = np.array(img)

    for a in range(0, arr.shape[0]):  # divide by the amount of pictures
        for b in range(0, arr.shape[1]):
            for c in range(0, arr.shape[2]):
                np_img[a, b, c] = np.float64(int(np_img[a, b, c]) / 255)  # float between 0 and 1

    arr = np.subtract(np_img, background)

    for a in range(0, arr.shape[0]):  # divide by the amount of pictures
        for b in range(0, arr.shape[1]):
            for c in range(0, arr.shape[2]):
                if arr[a, b, c] < 0:
                    arr[a, b, c] = 0

    create_classification_file(arr, i)


# plt.imshow(arr, interpolation='nearest')
# plt.show()
